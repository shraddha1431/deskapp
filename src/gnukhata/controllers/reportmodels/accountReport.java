/**
 * 
 */
package gnukhata.controllers.reportmodels;


public class accountReport {

	/**
	 * @param srNo
	 * @param accountName
	 * @param groupName
	 * @param subgroupName
	 */
	private String srNo;
	private String accountName;
	private String groupName;
	private String subgroupName;
	
	public accountReport(String srNo, String accountName, String groupName,
			String subgroupName) {
		super();
		this.srNo = srNo;
		this.accountName = accountName;
		this.groupName = groupName;
		this.subgroupName = subgroupName;
	}

	/**
	 * @return the srNo
	 */
	public String getSrNo() {
		return srNo;
	}

	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @return the subgroupName
	 */
	public String getSubgroupName() {
		return subgroupName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "accountReport [accountName=" + accountName + "]";
	}
	
	
	
}
